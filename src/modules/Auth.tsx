import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Alert
} from 'react-native';

import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({setUserID}: any) => {
    // If null, no SMS has been sent
    const [confirm, setConfirm] = React.useState<any>(null);
    const [code, setCode] = React.useState('');  
  
    // Handle the button press
    async function signInWithPhoneNumber(phoneNumber : any) {
      try {
        const confirmation : any = await auth().signInWithPhoneNumber(phoneNumber);
        setConfirm(confirmation);
      } catch (error) {
        Alert.alert(
          "Failed",
          error.message,
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      } 
    }
  
    async function confirmCode() {
      try {
        const responce = await confirm.confirm(code);
        setUserID(responce.user.uid)
        ////////////////////////////////////////////////
        //await AsyncStorage.setItem('@firebase_uid', responce.user.uid)
        ////////////////////////////////////////////////
      } catch (error) {
        Alert.alert(
          "Failed",
          error.message,
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      }
    }

  const [number, setNumber] = React.useState('')
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>You are Signed Out. Please input your phone number and continue with arrived SMS code</Text>
              {confirm ?  <>
                <Text style={styles.sectionDescription}>SMS code</Text>
                  <TextInput value={code} onChangeText={(text: any) => setCode(text)} placeholder = 'enter SMS code' />
                  <Button title="Confirm Code" onPress={() => confirmCode()} />
                  <Button title="back" onPress={() => setConfirm(false)} />
                </>
                :   
                <>
                  <Text style={styles.sectionDescription}>Phone number</Text>
                  <TextInput 
                    placeholder = {'enter number'}
                    value = {number}
                    onChangeText = {setNumber}
                  />
                  <Button
                    title="Send SMS"
                    onPress={() => signInWithPhoneNumber(number)} //reCaptcha required SHA-1
                  />
                </>
              }
            </View>

            <Text>Enter a new phone number (e.g. <Text style={styles.bold}>+44 7444 555666</Text>) and a test code (e.g. <Text style={styles.bold}>123456</Text>).</Text>

          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  bold : {
    fontWeight: "bold",
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  }
});

export default Home;
